/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package test

import GTAS.Predicate

/**
 * @author yuliu
 *
 */

class LengthLimit[T](n: Int) extends Predicate[T, Int] {
  def postProcess(a: Int) = (a <= n)
  def combine(l: Int, r: Int) = (l + r) min (n + 1)
  def f(a: T): Some[Int] = Some(1)
  val id: Int = 0
}

class LengthLimit2[T](n: Int) extends Predicate[T, Int] {
  var counter: Int = 0
  def postProcess(a: Int) = (counter <= n)
  def combine(l: Int, r: Int) = 0
  def f(a: T): Option[Int] = { counter += 1; None }
  val id: Int = 0
}

object testListhom extends App {

  def time(name: String, f: => Unit) = {
    val s = System.currentTimeMillis
    f
    val t = (System.currentTimeMillis - s)

    println("+++++ Function " + name + " takes " + t + "millis")

  }

  val x = List.range(0, 10000000, 1)
  val testLen1 = new LengthLimit[Int](9999)
  val testLen2 = new LengthLimit2[Int](9999)

  def f1 = {
    val rst = testLen1.compute(x)
    println(rst)
  }

  def f2 = {
    val rst = testLen2.compute(x)
    println(rst)
  }

  time("new", f2)
  time("mirrored", f1)

}
