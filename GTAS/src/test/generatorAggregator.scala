/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package test

import GTAS._
import DS.KnapsackItem
import Examples.KnapsackLocal

object generatorAggregator extends GTA[KnapsackItem, KnapsackItem] with App {

  def testLocal(lst: List[KnapsackItem]) {
    val itemA = new KnapsackItem(3, 2)
    println(itemA.value)

    val itemB = new KnapsackItem(2, 2)
    val x = List(new KnapsackItem(6, 1), itemA, itemB) ::: lst

    println("input = " + x)
    val gta1 = generate( new AllSelects[KnapsackItem] ) aggregate (new BagAggregator[KnapsackItem]())
    println("AllSelects of x = " + gta1.compute(x))
    println("AllSelects of x length= " + gta1.compute(x).asInstanceOf[Bag[KnapsackItem]].get.length)

    //generate + aggregate
    val test2 = generate(new AllSelects[KnapsackItem]()) aggregate (maxTotalValue)
    println("MaxTotalValue = " + test2.compute(x))

    //generate + filter + aggregate
    val test3 = generate(new AllSelects[KnapsackItem]()) ./* generator */
                   filter (new WeightLimit(4))           ./* filter    */
                       aggregate (maxTotalValue)          /*aggregator */
    println("MaxTotalValue with WeightLimit 4 = " + test3.compute(x))

    val getSolution = new SelectiveAggregator(maxTotalValue, new BagAggregator[KnapsackItem])
    val rst = generate(new AllSelects[KnapsackItem]()) filter (new WeightLimit(4)) aggregate (getSolution)
    println("The solution is " + rst.compute(x))

    
    //multi filter
    val test4 = generate(new AllSelects[KnapsackItem]())                  .
                         filter (new WeightLimit(6))                      . //filter 1           
                             filter (EvenNumberKnapsackItems)             . //filter 2
                                aggregate (maxTotalValue)
    println("MaxTotalValue with WeightLimit 6 and even # of items = " + test4.compute(x))
    
    //getSolution
    val test5 = generate(new AllSelects[KnapsackItem]()) filter (new WeightLimit(6)) filter (EvenNumberKnapsackItems) aggregate (getSolution)
    println("The selection is " + test5.compute(x))
 
    //multi filter
    val test6 = generate(new AllSelects[KnapsackItem]()) filter (new WeightLimit(6)) filter (new A_needs_B(itemA, itemB)) aggregate (maxTotalValue)
    println("MaxTotalValue with WeightLimit 6 and A=>B = " + test6.compute(x))
    //getSolution
    val test7 = generate(new AllSelects[KnapsackItem]()) filter (new WeightLimit(6)) filter (new A_needs_B(itemA, itemB)) aggregate (getSolution)
    println("The selection is " + test7.compute(x))
   
    //multi filter
    val test10 = generate(new AllSelects[KnapsackItem]()) filter (new WeightLimit(6)) filter (Ascending) aggregate (maxTotalValue)
    println("MaxTotalValue with WeightLimit 6 and ascending = " + test10.compute(x))
    //getSolution
    val test11 = generate(new AllSelects[KnapsackItem]()) filter (new WeightLimit(6)) filter (Ascending) aggregate (getSolution)
    println("The selection is " + test11.compute(x))
   
    //multi filter
    val test12 = generate(new AllSelects[KnapsackItem]()) .
                    filter (new WeightLimit(6))           .
                         filter (Ascending)               .
                               filter (new A_needs_B(itemA, itemB)) .
                                    aggregate (maxTotalValue)
                            
    println("MaxTotalValue with WeightLimit 6 and" + itemA + "=>" + itemB + " and Ascending= " + test12.compute(x))
    val test13 = generate(new AllSelects[KnapsackItem]()) filter (new WeightLimit(6)) filter (Ascending) filter (new A_needs_B(itemA, itemB)) aggregate (getSolution)
    println("The selection is " + test13.compute(x))
  }

  val lst = List(new KnapsackItem(2, 5), new KnapsackItem(1, 6))
  
  testLocal(lst)
  KnapsackLocal.test(lst) //Emoto version

}