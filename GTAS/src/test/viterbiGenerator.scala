/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package test

import GTAS._
import scala.collection.immutable.HashMap
import scala.collection.immutable.List

//object assTransGen extends GeneratorCreater[Int, Tuple2[Int, (Int, Int)]] {
//  val marks = List((1, 1), (2, 2), (3, 3))
//  override def makeGenerator[S](s: Aggregator[Tuple2[Int, (Int, Int)], S]): MapReduceable[Int, S, S] =
//    new MapReduceable[Int, S, S] {
//      def f(i: Int): Some[S] = Some(marks.foldLeft(s.zero)((z, mk) => s.plus(z, s.f(Tuple2(i, mk)).get)))
//      def combine(l: S, r: S): S = s.times(l, r)
//      val id: S = s.id
//      def postProcess(a: S): S = a
//    }
//}

//object Weather extends Enumeration{
//  val Sunny, Rainy, Cloudy = Value
//}
//
//object Action extends Enumeration{
//  val Shopping, Reading, Sleeping = Value
//}
  
object testViterbi extends mirrored.GTA[Action, Tuple2[Action, (Weather, Weather)],Id] with App{
  val states = Set(Sunny, Rainy, Cloudy)

  //type Transition = List[(Weather, Weather)]
  type MarkedTs = Tuple2[Action, (Weather, Weather)]
  type Mark =  Tuple2[Weather, Weather]

  val probs = new Probabilities[Action,Weather]{
    override  def envtProb(evn:Action, st:Weather):Double = st match{
      case  Sunny => evn match {
            case  Shopping => 0.8
            case  Reading  => 0.4
            case  Sleeping => 0.1
            case _ => throw new IllegalArgumentException("no possible")
      }
     case Rainy => evn match {
            case  Shopping => 0.1
            case  Reading  => 0.4
            case  Sleeping => 0.8
            case _ => throw new IllegalArgumentException("no possible")
      }
     case Cloudy => evn match {
            case  Shopping => 0.4
            case  Reading  => 0.8
            case  Sleeping => 0.2
            case _ => throw new IllegalArgumentException("no possible")
      }
     case _ => throw new IllegalArgumentException("no such things")
    }   
            
    override  def transProb(st1:Weather, st2:Weather):Double = st1 match {
      case Sunny => st2 match{
        case Sunny => 0.6
        case Rainy => 0.4
        case Cloudy => 0.4
        case _ => throw new IllegalArgumentException("no possible")
      }
       case Rainy => st2 match{
        case Sunny => 0.4
        case Rainy => 0.2
        case Cloudy => 0.7
        case _ => throw new IllegalArgumentException("no possible")
      }
     case Cloudy => st2 match{
        case Sunny => 0.4
        case Rainy => 0.7
        case Cloudy => 0.5
        case _ => throw new IllegalArgumentException("no possible")
      }
        case _ => throw new IllegalArgumentException("no such things")
    }

  }
  //tail recursion
  def chechTrans(a: Mark): Boolean = {
  if (a  == (NoState, NoState))
      false
    else
      true
  }

  object assTransGen extends mirrored.GeneratorCreater[Action, Tuple2[Action, (Weather, Weather)], Id] {
  val marks = for(x <- states ; y <-states) yield (x,y) // List(x,y) ??
  
  override def makeGenerator[S](s: Aggregator[Tuple2[Action, (Weather, Weather)], S]): MapReduceable[Action, Id[S], S] =
    new MapReduceable[Action, Id[S], S] {
      def f(i: Action): Some[Id[S]] = Some(marks.foldLeft(s.zero)((z, mk) => s.plus(z, s.f(Tuple2(i, mk)).get)))
      def combine(l: Id[S], r: Id[S]): Id[S] = s.times(l, r)
      val id: Id[S] = s.id
      def postProcess(a: Id[S]): S = a
    }
}
  
  object viterbiTest extends Predicate[MarkedTs, Mark] {
    /*
     * this looks not a list homomorphism
     */
    def postProcess(a: Mark) = chechTrans(a)
    def combine(l: Mark, r: Mark) = {
      if (l == (null, null)) r
      else {
        (l._2.toString() == r._1.toString()) match {
          case true => (l._1, r._2)
          case _ => Tuple2[Weather, Weather](NoState, NoState)
        }
      }
    }
    def f(a: MarkedTs): Option[Mark] = Some( a._2 )
    val id: Mark = Tuple2[Weather,Weather](null,null)
  }

  object viterbiAgg extends MaxProdAggregator[MarkedTs] {
   override def f(mts :MarkedTs):Some[Double] ={
     val event = mts._1
     val trans = mts._2
     val prob = probs.transProb(trans._1, trans._2) * probs.envtProb(event,trans._2)
     Some[Double](prob) 
   }
  }
  val gta = generate(assTransGen) filter (viterbiTest) aggregate(viterbiAgg)

  val events = List(Reading, Sleeping , Reading , Sleeping , Reading)
  
  val rst = gta.compute(events)
  
  println("viterbi: " + rst)
  
}
