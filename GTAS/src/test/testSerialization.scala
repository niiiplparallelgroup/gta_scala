/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package test


import GTAS._
import DS.KnapsackItem
import scala.collection.immutable.HashMap
import Examples.KnapsackSpark
import org.apache.spark.SparkContext


object testSerialization extends GTA[KnapsackItem,KnapsackItem] with App {

    val n = 1000
    
    val x = for (i <- List.range(1, n)) yield new Bag(List(List[Int](i, i + 1)))
 
    val spark = new SparkContext("local", "Spark_Knapsack", System.getenv("SPARK_HOME"), List(System.getenv("SPARK_EXAMPLES_JAR")))
    val slices = 2

    val rst = spark.parallelize(x, slices).map(e => e)
    println("input length is " + x.length)
    println("======" + rst.aggregate(0)((i, bg) => i + 1, (l, r) => l + r))

    val xx = for (i <- List.range(1, n)) yield HashMap[Int, Bag[Int]](i -> new Bag[Int](List(List[Int](i, i + 1))))
    val rst2 = spark.parallelize(xx, slices).map(e => e)
    println("input length is " + xx.length)
    println("+++HashMap++++" + rst2.aggregate(0)((i, bg) => i + 1, (l, r) => l + r))

    val lst = KnapsackSpark.creatTestData(n)
    val len = spark.parallelize(lst, slices).map(e => List(e.value, e.weight)).reduce(_ ::: _)
    println("knapsack list length*2 is = " + len.length)

    val generator = generate(new AllSelects[KnapsackItem]()) filter (new WeightLimit(100)) aggregate (maxTotalValue)

    val inter = spark.parallelize(lst, slices).map(generator.f(_).get).reduce(generator.combine(_, _))

    val rst3 = generator.postProcess(inter)

    println("knapsack AllSelects+ MaxTotalValue = " + rst3)

    println("check knapsack AllSelects+ MaxTotalValue = " + generator.compute(lst))
    
}