/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package Examples

import mirrored.GTA
import GTAS.Id
import GTAS.T4
import mirrored.SegmentsGen
import GTAS.BagAggregator
import GTAS.Bag
import GTAS.MaxTotalValueInt
import scala.util.Random
import GTAS.LengthLimit
import GTAS.EvenIntLengthLimit
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object mSsExamples extends GTA[Int, Int, T4] {

  def creatTestData(n: Int): List[Int] = {
    //create n knapsack items
    val rand = new Random()
    val rst = for (x <- List.range(0, n)) yield rand.nextInt(100)
    rst
  }

  // create test data in memory
  def createTestDataInMem(contex: SparkContext, slices: Int, length: Int): RDD[Int] = {
    val seed = List.range(0, slices * 2)
    val rdd = contex.parallelize(seed, slices).flatMap(e => creatTestData(length / (slices * 2)): List[Int])
    //println("input length = " + rdd.count + " slices=" + slices)
    rdd
  }

  def testSparkMSS(contex: SparkContext, x: RDD[Int]) = {

    val allSegs = new SegmentsGen[Int]
    val gta = generate(allSegs) aggregate (MaxTotalValueInt)
    val rst = gta.postProcess(x.map(gta.f(_).get).reduce(gta.combine(_, _)))
    //rst.asInstanceOf[Int]
    println("mss0++++++++++++++++++++++++++++++")
    (":maxmium segment sum = " + rst)
  }

  def testSparkExMSS1(contex: SparkContext, x: RDD[Int], len: Int) = {

    val allSegs = new SegmentsGen[Int]
    val lengthLimit = new LengthLimit[Int](len)
    val gta = generate(allSegs) filter (lengthLimit) aggregate (MaxTotalValueInt)
    val rst = gta.postProcess(x.map(gta.f(_).get).reduce(gta.combine(_, _)))
    println("mss1++++++++++++++++++++++++++++++")
    (": maxmium segment sum with lengthLimit= " + rst)
  }
  
    def testSparkExMSS2(contex: SparkContext, x: RDD[Int], len: Int) = {

    val allSegs = new SegmentsGen[Int]
    val limit = new EvenIntLengthLimit(len)
    val gta = generate(allSegs) filter (limit) aggregate (MaxTotalValueInt)
    val rst = gta.postProcess(x.map(gta.f(_).get).reduce(gta.combine(_, _)))
    println("mss2++++++++++++++++++++++++++++++")
    (": maxmium segment sum with number of even integers limit= " + rst)
  }
  

  def main(args: Array[String]): Unit = {

    val spark = new SparkContext(args(0), "Spark_MSS", System.getenv("SPARK_HOME"), List(System.getenv("MY_APPS") + "/gtas.jar"))
    if (args.length == 0) {
      System.err.println("Usage: mSsExamples <host> [<slices>] [<length of input-list>]")
      System.exit(1)
    }

    val slices = if (args.length > 1) args(1).toInt else 4
    val length = if (args.length > 2) args(2).toInt else 100

    val x = createTestDataInMem(spark, slices, length).cache
    println("+++++ generate all data => size=" + x.count + " +++++")

    val str1 = testSparkMSS(spark,x)
    
    val str2 = testSparkExMSS1(spark,x, 100)
    val str3 = testSparkExMSS2(spark,x, 100)
    println("=======")
    println("===1===" + str1)
    println("===2===" + str2)
    println("===3==="+ str3)
    System.exit(0)
  }

}
