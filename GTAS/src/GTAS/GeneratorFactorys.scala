/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package GTAS

/**
 * @author Yu Liu
 *
 */

import GTAS._
import scala.collection.immutable.HashMap

trait GFEM[I] {
  //old-version
  def makeGenerator[S](s: Aggregator[I, S]): MapReduceable[I, S, S]
}

 

trait GeneratorCreater[I, AI] extends BaseType {
  def makeGenerator[S](s: Aggregator[AI, S]): MapReduceable[I, S, Any]
}

