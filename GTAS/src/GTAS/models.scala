/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package GTAS

import DS.KnapsackItem
import scala.collection.Iterator
/*
 * cpoyed form Dr. Emoto
 * this class provide a function: makeGenerator
 * it limited type parameters of the ParalAccumlator[A,B,C], there must be B==C
 */
//TODO: delete
class AllSelects1[K] extends GFEM[K] {
  def makeGenerator[S](s: Aggregator[K, S]) = new MapReduceable[K, S, S] {
    override def f(i: K): Some[S] = Some(s.bagUnion(s.singleton(i).get, s.bagOfNil))
    override def combine(l: S, r: S): S = s.crossConcat(l, r)
    override def postProcess(a: S): S = a
    override val id: S = s.bagOfNil
  }
}

class AllSelects[K] extends GeneratorCreater[K, K] {

  def makeGenerator[S](s: Aggregator[K, S]) = new MapReduceable[K, S, S] {
    //S == R in this case
    override def f(i: K): Some[S] = Some(s.bagUnion(s.singleton(i).get, s.bagOfNil))
    override def combine(l: S, r: S): S = s.crossConcat(l, r)
    override def postProcess(a: S): S = a
    override val id: S = s.bagOfNil
  }
}

/*
 * This is a ParalAccumlator[K, (P, P), P], the 2nd type parameter's structure is bounded to Touple2(P,P)
 */

/*
 * list homo must be constructed by bag-semiring 
 * so that in the type parameters, must exist one type denotes the Bag[I] I-type and Bag[I]
 * s: Aggregator[I,B], B is (implicitly) Bag[I]
 */
class AssignTrans[I, V, Mark](val s: Aggregator[Tuple2[I, Mark], V], val marks: List[Mark]) extends MapReduceable[I, V, V] {
  def f(i: I): Some[V] = Some(marks.foldLeft(s.zero)((z, mk) => s.plus(z, s.f(Tuple2(i, mk)).get)))
  def combine(l: V, r: V): V = s.times(l, r)
  val id = s.id
  def postProcess(a: V): V = a
}

/*
 *  need to give a Concrete Aggregator on Pair, when use this one 
 */
class PrefixsGen[I] extends GeneratorCreater[I, Pair[I]] {
  def makeGenerator[S](s: Aggregator[Pair[I], S]) = new MapReduceable[I, S, S] {
    override def f(i: I) = Some(s.f(new Pair(i, i)).get)
    override def combine(l: S, r: S): S = s.plus(l, r)
    override val id: S = s.id
    override def postProcess(a: S): S = a
  }
}

class SegmentsGen[I] extends GeneratorCreater[I, T4[I]] {
  def makeGenerator[S](s: Aggregator[T4[I], S]) = new MapReduceable[I, S, S] {
    override def f(i: I) = Some(s.f(new T4(i, i, i, i)).get)
    override def combine(l: S, r: S): S = s.plus(l, r)
    override val id: S = s.id
    override def postProcess(a: S): S = a
  }
}

// assumption: S1 is selective: i.e., outerCombine(a, b) = a or b
class SelectiveAggregator[I, S1, S2](agg1: Aggregator[I, S1], agg2: Aggregator[I, S2]) extends Aggregator[I, (S1, S2)] {
  def plus(l: (S1, S2), r: (S1, S2)) = {
    val v1 = agg1.plus(l._1, r._1)
    if (v1 == r._1) r
    else l
  }
  def times(l: (S1, S2), r: (S1, S2)) = {
    val v1 = agg1.times(l._1, r._1)
    val v2 = agg2.times(l._2, r._2)
    (v1, v2)
  }
  def f(a: I) = Some((agg1.f(a).get, agg2.f(a).get))
  val id = (agg1.id, agg2.id)
  val zero = (agg1.id, agg2.id)
}

/*
 * tester:FinitePredicate
 * @notice: any practical test(s) should contains at least one FinitePredicate
 * limit to [0,w], w is included
 */
class WeightLimit(w: Int) extends FinitePredicate[KnapsackItem, Int] {
  override def postProcess(a: Int) = a <= w
  override def combine(l: Int, r: Int) = (l + r) min (w + 1)
  override def f(a: KnapsackItem): Some[Int] = Some(a.weight)
  override val id: Int = 0

  def start: Int = 0
  def end: Int = w + 1
  def findnext(thisOne: Int): Int = thisOne + 1

  override def iterator: Iterator[Int] = new Iterator[Int] {
    private var current = start - 1
    def next = {
      current += 1
      current
    }
    def hasNext = {
      val tmp = current
      tmp + 1 <= end
    }
  }

  override def count: Int = {
    var ct = 0
    while (findnext(ct) != end) {
      ct += 1
    }
    ct
  }

  // for performance reason
  override def foreach[U](f: Int => U) = {
    var it = 0
    while (it < end) {
      f(findnext(it))
      it += 1
    }
  }

  def apply(n: Int): WeightLimit = new WeightLimit(n)
}

/*
 * tester:FinitePredicate
 * @notice: any practical test(s) should contains at least one FinitePredicate
 * limit to [0,w], w is included
 */
class LengthLimit[T](n: Int) extends FinitePredicate[T, Int] {
  def postProcess(a: Int) = (a <= n)
  def combine(l: Int, r: Int) = (l + r) min (n + 1)
  def f(a: T): Some[Int] = Some(1)
  val id: Int = 0

  //this test space is infinite
  def start: Int = 0
  def end: Int = n + 1
  // this needs monotonous 
  def findnext(thisOne: Int): Int = thisOne + 1
  def count: Int = {
    var ct = 0
    // this needs monotonous 
    while (findnext(ct) != end) {
      ct += 1
    }
    ct
  }

  override def iterator: Iterator[Int] = new Iterator[Int] {
    private var current = start - 1
    def next = {
      current += 1
      current
    }
    def hasNext = {
      val tmp = current
      tmp + 1 <= end
    }
  }

  // for performance reason
  override def foreach[U](f: Int => U) = {
    var it = 0
    while (it < end) {
      f(findnext(it))
      it += 1
    }
  }
}

class EvenIntLengthLimit(n: Int) extends LengthLimit[Int](n) {
  override def f(a: Int): Some[Int] = if ((a % 2) == 0) Some(1) else Some(0)
}

class A_needs_B(a: KnapsackItem, b: KnapsackItem) extends Predicate[KnapsackItem, (Boolean, Boolean)] {
  def postProcess(t: (Boolean, Boolean)) = !t._1 || (t._1 && t._2)
  def combine(l: (Boolean, Boolean), r: (Boolean, Boolean)) = (l._1 || r._1, l._2 || r._2)
  def f(i: KnapsackItem) = Some((i == a, i == b))
  val id = (false, false)
}

class EvenLength[T] extends Predicate[T, Int] {
  def postProcess(a: Int) = (a % 2) == 0
  def combine(l: Int, r: Int) = l + r
  def f(a: T): Some[Int] = Some(1)
  val id: Int = 0
}

abstract class MaxSum[T] extends Aggregator[T, Int] {
  def plus(l: Int, r: Int) = l max r
  def times(l: Int, r: Int) = l + r
  def f(a: T): Some[Int]
  val id: Int = 0
  val zero: Int = Int.MinValue // -2147483648 == -infty
}

abstract class MaxProdAggregator[T] extends Aggregator[T, Double] {
  def plus(l: Double, r: Double) = l max r
  def times(l: Double, r: Double) = l * r
  def f(a: T): Option[Double]
  val id: Double = 1.0
  val zero: Double = 0.0
}

trait Probabilities[Event, State] {
  def envtProb(evn: Event, st: State): Double
  def transProb(st1: State, st2: State): Double
}
///////////////////////////////////////////////////////////////////////////////////////////////

object allSelects extends AllSelects[Int]

object evenItems extends EvenLength[Int]

// asc values
object Ascending extends Predicate[KnapsackItem, (Int, Boolean, Int)] {
  def postProcess(t: (Int, Boolean, Int)) = t._2
  def combine(l: (Int, Boolean, Int), r: (Int, Boolean, Int)) = if (l._1 == -1) r else if (r._1 == -1) l else
    (l._1, l._2 && (l._3 <= r._1) && r._2, r._3)
  def f(i: KnapsackItem) = Some((i.weight, true, i.weight))
  val id = (-1, true, 0) // special
}

// this can be written as a FinitePredicate
object EvenNumberKnapsackItems extends EvenLength[KnapsackItem]

object maxSum extends MaxSum[Int] {
  override def f(a: Int): Some[Int] = Some(a)
}

object maxTotalValue extends Aggregator[KnapsackItem, Int] {
  def plus(l: Int, r: Int) = l max r
  def times(l: Int, r: Int) = l + r
  def f(a: KnapsackItem) = Some(a.value)
  val id: Int = 0
  val zero: Int = Int.MinValue // -2147483648 == -infty
}

object MaxTotalValueInt extends Aggregator[Int, Int] {
  def plus(l: Int, r: Int) = l max r
  def times(l: Int, r: Int) = l + r
  def f(a: Int) = Some(a)
  val id: Int = 0
  val zero: Int = Int.MinValue // -2147483648 == -infty
}



