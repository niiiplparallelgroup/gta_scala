
/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package GTAS.EMOTO

import GTAS.Aggregator
import GTAS.GFEM
import GTAS.Id
import GTAS.Predicate


trait GTAEM {
  def generate[I,GG <: GFEM[I]](x: List[I], gg: GG) =
    new GTAComputationLocal[I, GG, Id](x, gg) {
      //semiring fusion
      def build[S](s: Aggregator[I, S]) =
        new Aggregator[I, Id[S]] {
          override def plus(l: Id[S], r: Id[S]) =
            new Id[S](s.plus(l.value, r.value))
          override def times(l: Id[S], r: Id[S]) =
            new Id[S](s.times(l.value, r.value))
          override def f(a: I) = Some(new Id[S](s.f(a).get))
          override val id: Id[S] = new Id[S](s.id)
          override val zero: Id[S] = new Id[S](s.zero)
        }
      override def postProcess[S](s: Aggregator[I, S], a: Id[S]) = a.value
    }

}

trait GTAComputationTLocal[I, GG <: GFEM[I], Lift[_]] {
  val x: List[I]
  val gg: GG
  def aggregate[S](agg: Aggregator[I, S]) = {
    postProcess(agg, gg.makeGenerator(build(agg)).compute(x))
  }
  def build[S](s: Aggregator[I, S]): Aggregator[I, Lift[S]]
  def postProcess[S](s: Aggregator[I, S], a: Lift[S]): S
}

abstract class GTAComputationLocal[I, GG <: GFEM[I], Lift[_]](_x: List[I], _gg: GG) extends GTAComputationTLocal[I, GG, Lift] {
 //(implicit m:Manifest[Lift[_]])
  val x = _x
  val gg = _gg
  //def hoge = println("ok")
  def filter[T](p: Predicate[I, T]) = {
    val oldGTA = this
    type NewLift[A] = Lift[Map[T, A]]
    new GTAComputationLocal[I, GG, NewLift](x, gg) { //hom_lifted_semiring
      def build[S](s: Aggregator[I, S]) =
        oldGTA.build[Map[T, S]](oneLift(s))
      def oneLift[S](s: Aggregator[I, S]) =
        new Aggregator[I, Map[T, S]] {
          def plus(l: Map[T, S], r: Map[T, S]) = {
            var m = scala.collection.mutable.Map[T, S]()
            l foreach (ll => m(ll._1) = ll._2)
            r foreach (rr => m(rr._1) = s.plus(m.getOrElse(rr._1, s.zero), rr._2))
            m.toMap
          }
          def times(l: Map[T, S], r: Map[T, S]) = {
            var m = scala.collection.mutable.Map[T, S]()
            l foreach (ll =>
              r foreach (rr => {
                val k = p.combine(ll._1, rr._1)
                val v = s.times(ll._2, rr._2)
                m(k) = s.plus(m.getOrElse(k, s.zero), v)
              }))

            m.toMap // make it immutable!
          }
          def f(a: I) = Some(Map(p.f(a).get -> s.f(a).get))
          val id: Map[T, S] = Map(p.id -> s.id)
          val zero: Map[T, S] = Map.empty[T, S]
        }
      def postProcess[S](s: Aggregator[I, S], a: Lift[Map[T, S]]) = {
        val m = oldGTA.postProcess[Map[T, S]](oneLift(s), a)
        m.foldLeft(s.zero)((l, kv) => if (p.postProcess(kv._1)) s.plus(l, kv._2) else l)
      }
    }
  }
}
